# Blade number detection demo

Blade number detection demo is a tool for detecting numbers from images taken from turbine blades and displaying these numbers as a string.

Here below you see how the numbers on an image are detected and result is presented
<br/>
<img src="https://gitlab.com/nrozeboom/blade-number-detection-demo/raw/master/example/result.jpg"/>